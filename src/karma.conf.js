// Karma configuration
// Generated on Fri Dec 16 2016 13:09:51 GMT+0000 (UTC)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

	frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
		require('karma-jasmine'),
		require('karma-chrome-launcher'),
		require('karma-jasmine-html-reporter'),
//		require('karma-coverage-istanbul-reporter'),
		require('@angular-devkit/build-angular/plugins/karma')
	],
	client: {
		clearContext: false
	},
	coverageIstanbulReporter: {
		dir: require('path').join(__dirname, '../coverage'),
		reports: ['html', 'lcovonly'],
		fixWebpackSourcePaths: true
	},
	reporters: ['progress', 'kjhtml'],
	port: 9876,
	colors: true,
	logLevel: config.LOG_INFO,
	autoWatch: true,
	browsers: ['ChromeHeadlessNoSandbox'],
	customLaunchers: {
		ChromeHeadlessNoSandbox: {
			base: 'ChromeHeadless',
			flags: ['--no-sandbox']
		}
	},
	singleRun: false
  });
};
